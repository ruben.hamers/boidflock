using System;
using UnityEngine;

namespace HamerSoft.BoidFlock
{
    public class HsObject : MonoBehaviour
    {
        public event Action<HsObject> Started, Destroyed;

        protected virtual void Awake()
        {
        }

        protected virtual void Start()
        {
            Started?.Invoke(this);
        }

        protected virtual void OnDestroy()
        {
            Destroyed?.Invoke(this);
        }
    }
}