﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour
{
    public Vector3 Position { get; private set; }
    public Vector3 Forward { get; private set; }
    private Vector3 _avgFlockHeading;
    private Vector3 _avgAvoidanceHeading;
    private Vector3 _centreOfFlockmates;
    private int _numPerceivedFlockmates;
    private Vector3 _velocity, _acceleration;
    private Material _material;
    private Transform _cachedTransform;
    private Transform _target;
    private BoidSettings _settings;
    private const int _numViewDirections = 300;
    private static Vector3[] _directions;

    void Awake()
    {
        _material = transform.GetComponentInChildren<Renderer>().material;
        _cachedTransform = transform;
        if (_directions == null)
            GenerateDirections();
    }

    static void GenerateDirections()
    {
        _directions = new Vector3[_numViewDirections];
        float goldenRatio = (1 + Mathf.Sqrt(5)) / 2;
        float angleIncrement = Mathf.PI * 2 * goldenRatio;
        for (int i = 0; i < _numViewDirections; i++)
        {
            float t = (float) i / _numViewDirections;
            float inclination = Mathf.Acos(1 - 2 * t);
            float azimuth = angleIncrement * i;
            float x = Mathf.Sin(inclination) * Mathf.Cos(azimuth);
            float y = Mathf.Sin(inclination) * Mathf.Sin(azimuth);
            float z = Mathf.Cos(inclination);
            _directions[i] = new Vector3(x, y, z);
        }
    }

    public void Initialize(BoidSettings settings, Transform target)
    {
        this._target = target;
        this._settings = settings;

        Position = _cachedTransform.position;
        Forward = _cachedTransform.forward;

        float startSpeed = (settings.minSpeed + settings.maxSpeed) / 2;
        _velocity = transform.forward * startSpeed;
    }

    public void SetColour(Color col)
    {
        if (_material != null)
        {
            _material.color = col;
        }
    }

    public void UpdateFlockProperties(Vector3 flockHeading, Vector3 flockCentre, Vector3 avoidanceHeading,
        int numFlockmates)
    {
        _avgFlockHeading = flockHeading;
        _avgAvoidanceHeading = avoidanceHeading;
        _centreOfFlockmates = flockCentre;
        _numPerceivedFlockmates = numFlockmates;
    }

    public void UpdateBoid()
    {
        Vector3 acceleration = Vector3.zero;

        if (_target != null)
        {
            Vector3 offsetToTarget = (_target.position - Position);
            acceleration = SteerTowards(offsetToTarget) * _settings.targetWeight;
        }

        if (_numPerceivedFlockmates != 0)
        {
            _centreOfFlockmates /= _numPerceivedFlockmates;

            Vector3 offsetToFlockmatesCentre = (_centreOfFlockmates - Position);

            var alignmentForce = SteerTowards(_avgFlockHeading) * _settings.alignWeight;
            var cohesionForce = SteerTowards(offsetToFlockmatesCentre) * _settings.cohesionWeight;
            var seperationForce = SteerTowards(_avgAvoidanceHeading) * _settings.seperateWeight;

            acceleration += alignmentForce;
            acceleration += cohesionForce;
            acceleration += seperationForce;
        }

        if (IsHeadingForCollision())
        {
            Vector3 collisionAvoidDir = ObstacleRays();
            Vector3 collisionAvoidForce = SteerTowards(collisionAvoidDir) * _settings.avoidCollisionWeight;
            acceleration += collisionAvoidForce;
        }

        _velocity += acceleration * Time.deltaTime;
        float speed = _velocity.magnitude;
        Vector3 dir = _velocity / speed;
        speed = Mathf.Clamp(speed, _settings.minSpeed, _settings.maxSpeed);
        _velocity = dir * speed;

        _cachedTransform.position += _velocity * Time.deltaTime;
        _cachedTransform.forward = dir;
        Position = _cachedTransform.position;
        Forward = dir;
    }

    private bool IsHeadingForCollision()
    {
        RaycastHit hit;
        if (Physics.SphereCast(Position, _settings.boundsRadius, Forward, out hit, _settings.collisionAvoidDst,
            _settings.obstacleMask))
        {
            return true;
        }
        else
        {
        }

        return false;
    }

    private Vector3 ObstacleRays()
    {
        for (int i = 0; i < _directions.Length; i++)
        {
            Vector3 dir = _cachedTransform.TransformDirection(_directions[i]);
            Ray ray = new Ray(Position, dir);
            if (!Physics.SphereCast(ray, _settings.boundsRadius, _settings.collisionAvoidDst, _settings.obstacleMask))
            {
                return dir;
            }
        }

        return Forward;
    }

    private Vector3 SteerTowards(Vector3 vector)
    {
        Vector3 v = vector.normalized * _settings.maxSpeed - _velocity;
        return Vector3.ClampMagnitude(v, _settings.maxSteerForce);
    }
}